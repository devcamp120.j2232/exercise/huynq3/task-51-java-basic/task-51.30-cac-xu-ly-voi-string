import java.util.ArrayList;

public class Task5130 {
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        //task 1
        String string1 = "bananas";
        System.out.println("Task 1 - original string: " + string1);
        StringBuilder newString1 = new StringBuilder(string1);
        int len1 = string1.length();
        for (int i=0; i < len1; i++){
            for (int j=0; j < i; j++){
                if (string1.charAt(i)== string1.charAt(j)){
                    newString1.deleteCharAt(i);
                    len1--;
                }
            }
        }
        System.out.println("Task 1 - new string: " + newString1);
        //task 2
        String string2 = "Abc  abc bac";
        int numOfWords = -1;
        System.out.println("Task 2 - String: " + string2);
        if(string2 == null || string2.isEmpty()){
            numOfWords = 0;
        } else {
            String[] words = string2.split("\\s+");
            numOfWords = words.length;
        }
        System.out.println("Task 2 - Count of words: " + numOfWords);
        //task 3
        String string3 = "DevCamp java";
        String string3new = "java";
        System.out.println("Task 3 - Concatenate string : " + string3.concat(string3new));
        //task 4
        System.out.println("Task 4 - String contains substring: " + string3.contains(string3new));
        //task 5
        int index = 3;
        System.out.println("Task 5 - Character at index in a string: " + string3.charAt(index-1));
        //task 6
        int count6 = 0;
        char ch = 'a';
        for (int i=0; i<string3.length(); i++){
            if (string3.charAt(i) == ch){
                count6++;
            }
        }
        System.out.println("Task 6 - Number of character " + ch + "in a string: " + count6);
        //task 7
        System.out.println("Task 7 - Index of a character in a string: " + string3.indexOf(ch));//kiểm tra lại để get position, not index
        //task 8
        System.out.println("Task 8 - Index of a character in a string: " + string3.toUpperCase());
        //task 9
        StringBuffer newStr9=new StringBuffer(string3);    
        for(int i = 0; i < string3.length(); i++) {    
            //Checks for lower case character    
            if(Character.isLowerCase(string3.charAt(i))) {    
                //Convert it into upper case using toUpperCase() function    
                newStr9.setCharAt(i, Character.toUpperCase(string3.charAt(i)));    
            }    
            //Checks for upper case character    
            else if(Character.isUpperCase(string3.charAt(i))) {    
                //Convert it into upper case using toLowerCase() function    
                newStr9.setCharAt(i, Character.toLowerCase(string3.charAt(i)));    
            }    
        }    
        System.out.println("Task 9 - String after case conversion : " + newStr9); 
        //task 10
        int count10 = 0;
        for(int i = 0; i < string3.length(); i++) {    
            //Checks for upper case character    
            if(Character.isUpperCase(string3.charAt(i))) {
                count10++;
            }
        }
        System.out.println("Task 10 - Number of upper case in string: " + count10);
        //Task 11
        System.out.println("Task 11 - Character from A to Z in string: ");
        for(int i = 0; i < string3.length(); i++) {    
            //Checks for upper case character    
            if(Character.isUpperCase(string3.charAt(i))) {
                System.out.println(string3.charAt(i));
            }
        }
        //Task 12
        String string12 = "abcdefghijklmnopqrstuvwxy";
        int len12 = 5;
        ArrayList<String> strings = new ArrayList<String>();
        int index12 = 0;
        while (index12 < string12.length()) {
            strings.add(string12.substring(index12, Math.min(index12 + len12, string12.length())));
            index12 += len12;
        }
        System.out.println("Task 12 - new string: " + strings);
        //task 13
        String string13 = "aabaarbarccrabmq";
        System.out.println("Task 13 - original string: " + string13);
        StringBuilder newString13 = new StringBuilder(string13);
        for (int i=0; i < string13.length()-1; i++){
            if (string13.charAt(i)== string13.charAt(i+1)){
                newString13.deleteCharAt(i);
            }
        }
        System.out.println("Task 13 - new string: " + newString13);
        //task 14
        String string14a = "Welcome";
        String string14b = "home";
        String string14c = " ";
        if (string14a.length() > string14b.length()){
            int dif = string14a.length()-string14b.length();
            string14c = string14a.substring(dif,dif+1);
        }
        else string14c = string14a;
        String string14New = string14c.concat(string14b);
        System.out.println("Task 14 - new string: " + string14New);
        //task 15
        StringBuffer newStr15=new StringBuffer(string3);    
        for(int i = 0; i < string3.length(); i++) {    
            //Checks for lower case character    
            if(Character.isLowerCase(string3.charAt(i))) {    
                //Convert it into upper case using toUpperCase() function    
                newStr15.setCharAt(i, Character.toUpperCase(string3.charAt(i)));    
            }    
            //Checks for upper case character    
            else if(Character.isUpperCase(string3.charAt(i))) {    
                //Convert it into upper case using toLowerCase() function    
                newStr15.setCharAt(i, Character.toLowerCase(string3.charAt(i)));    
            }    
        }    
        System.out.println("Task 15 - String after case conversion : " + newStr15);
        //task 16
        String string16 = "DevCamp123";
        boolean vCheck16 = false;
        for (int i = 0; i < string16.length(); i++) {
            // Check if character is
            // a digit between 0-9
            // then return true
            if (string16.charAt(i) >= '0' && string16.charAt(i) <= '9') {
                vCheck16 = true;
            }
        }
        System.out.println("Task 16 - String contains number: " + vCheck16);
        //task 17
        boolean vCheck17 = true;
        int len17 = string16.length();
        if (len17>20
        || (string16.charAt(0)<'A' && string16.charAt(0)>'Z')
        || (string16.charAt(len17-1) < '0' && string16.charAt(len17-1) > '9') ){
            vCheck17 = false;
        }
        else {
            for (int i = 0; i<len17; i++){
                if (string16.charAt(i) == ' '){
                    vCheck17 = false;
                }
            }
        }
        System.out.println("Task 17 - String validation result: " + vCheck17);  
    }
}
